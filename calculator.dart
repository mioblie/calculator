import 'dart:io';

class calculator {
  double? x;
  double? y;

  Calculator(double x, double y) {
    this.x = x;
    this.y = y;
  }

  add(double x, double y) {
    return x + y;
  }

  minus(double x, double y) {
    return x - y;
  }

  multiply(double x, double y) {
    return x * y;
  }

  divide(double x, double y) {
    return x / y;
  }

  percent(double x, double y) {
    return x % y;
  }
}
